import * as Docker from 'dockerode'
import { ContainerCreateOptions, Container } from 'dockerode'

class Dockerode extends Docker {

  // @ts-ignore
  private streamify (func) {
    return new Promise((resolve, reject) => {
      // @ts-ignore
      func((error, stream) => {
        if (error) {
          return reject(error)
        }
        const onEnd = (error: any) => {
          console.log('')
          if(error) {
            reject(error)
          } else {
            resolve()
          }
        }
        const events = {}
        // @ts-ignore
        const onProgress = (event) => {
          if (event && event.progressDetail && event.progressDetail.current) {
            // @ts-ignore
            events[event.id] = event.progressDetail
            const current = Object.keys(events).reduce(
              // @ts-ignore
              (total, event) => total + events[event].current,
              0
            )
            const total = Object.keys(events).reduce(
              // @ts-ignore
              (total, event) => total + events[event].total,
              0
            )
            const percentage = Math.round(100*current/total)
            if (!isNaN(percentage)) {
              const progress = `${current} / ${total} (${percentage}%)`
              process.stdout.write(`  ${progress}                             \r`)
            }
          }
        }
        this.modem.followProgress(stream, onEnd, onProgress)
      })
    }) 
  }
  
  createContainer (options: ContainerCreateOptions): Promise<Container> {
    if (options.Image) {
      return this.pullImage(options.Image).then(() => {
        return super.createContainer(options)
      })
    } else {
      return super.createContainer(options)
    }
  }

  pullImage (repoTag: string) {
    if (!repoTag.includes(':')) {
      repoTag = `${repoTag}:latest`
    }
    return this.streamify((handleStream: Function) =>
      super.pull(repoTag, handleStream)
    ).then(() =>
      this.getImage(repoTag)
    )
  }
}

export default Dockerode
